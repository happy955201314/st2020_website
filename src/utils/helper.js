const math = require("mathjs")
let sorting = (array) => {
    array.sort(compare)
    //console.log(array)
    return array;
}

let compare = (a, b) => {
    if(a["PM2.5"] == undefined) return a - b
    else return a["PM2.5"] - b["PM2.5"]
}

let average = (nums) => {
    const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length
    const avg = math.round(average(nums),2)
    //console.log(avg)
	   return avg;
}


module.exports = {
    sorting,
    compare,
    average
}